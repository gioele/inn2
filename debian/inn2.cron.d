SHELL=/bin/sh
PATH=/usr/lib/news/bin:/sbin:/bin:/usr/sbin:/usr/bin

# Expire old news and overview entries nightly, generate reports.

15 4	* * * 	news	test -x /usr/lib/news/bin/news.daily && news.daily expireover lowmark delayrm

# Refresh the cached IP addresses every day.

2 3	* * *	news	[ -x /usr/sbin/ctlinnd ] && ctlinnd -t 300 -s reload incoming.conf "flush cache"

# Every hour, run an rnews -U. This is not only for UUCP sites, but
# also to process queud up articles put there by nnrpd in case
# innd wasn't accepting any articles.

10 *	* * *	news	[ -x /usr/bin/rnews ] && rnews -U

# Enable this entry to send posted news back to your upstream provider.
# Also edit /etc/news/nntpsend.ctl !
# Not if you use innfeed, of course.

#*/15 *	* * *	news	nntpsend


# Enable this if you want to send news by uucp to your provider.
# Also edit /etc/news/send-uucp.cf !

#22 *	* * * 	news	send-uucp.pl

# NINPATHS ###################################################################
# To enable ninpaths please add this line to /etc/news/newsfeeds:
#   inpaths!:*:Tc,WP:/usr/lib/news/bin/ginpaths2
#
#6 6	* * *	news	ctlinnd -s -t 60 flush inpaths!
#8 6	1 * *	news	sendinpaths
# NINPATHS ###################################################################

